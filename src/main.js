import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import SuperFlow from 'vue-super-flow'
import 'element-ui/lib/theme-chalk/index.css'
import 'vue-super-flow/lib/index.css'

Vue.use(SuperFlow)
Vue.use(ElementUI)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
